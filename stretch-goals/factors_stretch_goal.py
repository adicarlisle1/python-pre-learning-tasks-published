def factors(number):
    # ==============
    answer = []
    for i in range(2,number): # ignoring 1 and N
        if number % i == 0:
            answer.append(i)
    
    if len(answer) == 0: 
        return f"{number} is a prime number"
    else:
        return answer
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
