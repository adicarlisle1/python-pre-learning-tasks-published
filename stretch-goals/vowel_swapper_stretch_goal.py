#### Work in progress
def vowel_swapper(string):
    # ==============
    dictionary = { # unfortunately redundant
        "a":"4",
        "A":"4",
        "e":"3",
        "E":"3",
        "i":"!",
        "I":"!",
        "o":"ooo",
        "O":"000",
        "u":"|_|",
        "U":"|_|"
        }
    countA=0
    countE=0
    countI=0
    countO=0
    countU=0

    stringGen =[]
    for char in string:
        if char in ["a","A"]:
            countA +=1
            helper(countA,char,dictionary,stringGen)
        elif char in ["e","E"]:
            countE +=1
            helper(countE,char,dictionary,stringGen)
        elif char in ["i","I"]:
            countI +=1
            helper(countI,char,dictionary,stringGen)
        elif char in ["o","O"]:
            countO +=1
            helper(countO,char,dictionary,stringGen)
        elif char in ["u","U"]:
            countU +=1
            helper(countU,char,dictionary,stringGen)
        else:
            stringGen.append(char)
    return ''.join(stringGen)
        
def helper(countType,char,dictionary,stringGen):
    if countType % 2 != 0:
        stringGen.append(char)
    else:
        stringGen.append(dictionary[char])

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
